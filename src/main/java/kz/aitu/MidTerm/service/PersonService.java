package kz.aitu.MidTerm.service;

import kz.aitu.MidTerm.entity.Person;
import kz.aitu.MidTerm.repository.PersonRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class PersonService {
    PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }
    public List<Person> getAll(){
        return (List<Person>) personRepository.findAll();
    }

    public void deleteById(Long id){
        personRepository.deleteById(id);
    }
    public Person updateById(@RequestBody Person person){
        return personRepository.save(person);
    }


}
