package kz.aitu.MidTerm.controller;

import kz.aitu.MidTerm.entity.Person;
import kz.aitu.MidTerm.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;

@RestController
public class PersonController {
    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }
    @GetMapping(path = "/api/v2/users")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(personService.getAll());
    }
    @DeleteMapping(path = "/api/v2/users/{}")
    public void deleteById(@PathVariable Long id){
        personService.deleteById(id);
    }
    @PostMapping(path = "/api/v2/users")
    public ResponseEntity<?> createPerson(@RequestBody Person person){
        return ResponseEntity.ok(personService.updateById(person));
    }

}
