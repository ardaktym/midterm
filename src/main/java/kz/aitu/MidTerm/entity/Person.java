package kz.aitu.MidTerm.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="person")
public class Person {
    @Id
    private Long id;
    @Column
    private String firstname;
    private String lastname;
    private String city;
    private String phone;
    private String telegram;

}
