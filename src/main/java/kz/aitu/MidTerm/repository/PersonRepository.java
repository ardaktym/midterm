package kz.aitu.MidTerm.repository;

import kz.aitu.MidTerm.entity.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person , Long> {
}
